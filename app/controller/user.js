const { sign } = require("jsonwebtoken");
// const secret = "demo";
const { findUserServe, findUserWeb } = require("../modal/user");
const { SECRET } = require("../../config/constant");

/**
 * 用户登录接口
 * @param {username} 用户名
 * @param {password} 用户密码
 */
const login = async (ctx) => {
  const request = ctx.request.body;
  const { username, password } = request;
  if (username) {
    let res = await findUserServe({ user_id: username });
    if (res.result && res.data.length == 0) {
      ctx.res.$error("用户不存在", 404);
    } else if (res.result && res.data[0].password != password) {
      ctx.res.$error("密码不正确", 404);
    } else if (res.result) {
      const token = sign({ user_id: username, password }, SECRET, {
        expiresIn: "24h",
      });
      ctx.res.$success({ token, ...res.data[0] });
    } else {
      ctx.res.$error("请求失败");
    }
  } else {
    ctx.res.$error("请输入用户名和密码", 400);
  }
};

/**
 * 获取用户信息 | 验证用户是否登录
 * @param {username?} 用户名可选,
 * 传则返回对应用户信息,不传默认返回token中用户信息
 */
const userInfo = async (ctx) => {
  const request = ctx.request.body;
  const { user_id } = request;
  let params = { user_id: ctx.state.user.user_id };
  if (user_id) {
    params.user_id = user_id;
  }
  let res = await findUserWeb(params);
  if (res.result && res.data.length == 0) {
    ctx.res.$error("用户不存在", 404);
  } else if (res.result) {
    ctx.res.$success(res.data[0]);
  } else {
    ctx.res.$error("请求失败");
  }
};

/**
 * 获取用户列表
 * @param {*} ctx
 */
const userList = async (ctx) => {
  const request = ctx.request.body;
  const params = ({ user_id, role } = request);
  let res = await findUserWeb(params);
  if (res.result) {
    ctx.res.$success(res.data);
  } else {
    ctx.res.$error("请求失败");
  }
};

/**
 * 注册用户
 */
const addUser = async (ctx) => {
  const request = ctx.request.body;
  const { user_id, user_name, user_sex, user_age, user_phone, dept_id, role } =
    request;
  if (user_id) {
    let data = await findUserWeb({ user_id });
    if(data.result && data.data.length !==0 ) {
      ctx.res.$error('用户已存在')
    }
    const queryData = {
      user_id,
      user_name,
      user_sex,
      user_age,
      user_phone,
      dept_id,
      role,
    };
    const res = await query(user.add('user'), queryData);
    if (res.result) {
      ctx.res.$success(res.data);
    } else {
      ctx.res.$error("请求失败");
    }
  } else {
    ctx.res.$error("缺少user_id", 400);
  }
};

/**
 * 
 */

module.exports = {
  login,
  userInfo,
  userList,
  addUser,

};
