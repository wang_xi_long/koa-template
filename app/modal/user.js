const { user } = require("../../dbhelper/user");
const { query } = require("../../config/dbPool");

const findUserServe = async (target) => {
  let res = JSON.parse(JSON.stringify(await query(user.query(target))));
  return res;
};

const findUserWeb = async (
  target = null,
  replacer = "dept_name,user_age,role,user_id,user_name,user_phone,user_sex"
) => {
  let res = JSON.parse(
    JSON.stringify(await query(user.query(target, replacer)))
  );
  return res;
};

module.exports = {
  findUserServe,
  findUserWeb,
};
