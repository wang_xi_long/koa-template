
/**
 * 字符串去空
 * @param {*} value 字符串值
 * @returns 收尾去空的字符串
 * 用法: let string = trim(value)
 */
const trim = (value) => {
    if(typeof value === "string") {
        return value.replace(/^\s+|\s+$/g,"")
    }
    return value;
}








module.exports = {
    trim
};