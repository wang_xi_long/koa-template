// 1. 引入动态SQL类
const { SQL } = require("../utils/sqlhandle");

// 2. 实现派生类
class usersSql extends SQL {
  super(option) {
    this.option = option;
  }
}

// 3. 实例user派生类
const user = new userSql({ table: "user,dept", connect: "dept_id" });

// 4. 导出实例user
module.exports = {
  user,
};
