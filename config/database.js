/**
 * 数据库配置文件
 */
const MYSQL_CONFIG = {
  host: "localhost", // 可以是本地地址，也可以设置成远程地址
  user: "root", // 数据库用户名
  password: "root", // 数据库密码
  port: "3306", // 数据库端口
  database: "mysql_test", // 数据库名
};
module.exports = MYSQL_CONFIG;

