const router = require("koa-router")();

router.prefix("/user");

const { SECRET } = require("../../config/constant");
const jwt = require("koa-jwt")({ secret: SECRET });

router.get("/", function (ctx, next) {
	ctx.body = "this a users response!";
});

const {
	login,
	userInfo,
	userList,
	addUser,
} = require("../../app/controller/user");


router.post("/", function (ctx, next) {
	ctx.body = "this a user response!";
});

router.post("/login", login);
router.get("/info", jwt, userInfo);
router.get("/validateLogin", jwt, userInfo);
router.post("/list", jwt, userList);
router.post("/register", jwt, addUser);

module.exports = router;
